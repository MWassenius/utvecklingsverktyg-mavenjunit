//Mikel Wassenius Java18
//Testa för CI

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.RepeatedTest;
import org.junit.jupiter.api.Test;


public class BlackjackTest {
	public Deck deck = new Deck();
	
	
//	Skapa en ny kortlek innan varje test
	@BeforeEach
	public void resetDeck() {
		deck = new Deck();
		
	}
	
//	Testa att det är 52 kort i en nyinitierad lek samt 	att det fortfarande är 52 när den blandas
	@Test
	public void testShuffle52() {
		assertEquals(52, deck.getCards().size(), "Kort i leken innan blanding är 52");
		deck.shuffle();
		assertEquals(52, deck.getCards().size(), "Kort i leken efter blanding är 52");
	}
	
//	Testa att det funkar även när ett kort dragits
	@Test
	public void testShuffleDraw() {
		deck.draw();
		assertEquals(51, deck.getCards().size(), "Kort i leken innan blanding är 51");
		deck.shuffle();
		assertEquals(51, deck.getCards().size(), "Kort i leken efter blanding är 51");
		
	}
	
//	Testa om dubletter finns i leken
	@Test
	public void testDoubles() {
		ArrayList<Card> cards = deck.getCards();
		for (int i = 0; i < 51; i++) {
			int counter=i+1;
			while (counter < 52) {
				assertNotEquals(cards.get(i), cards.get(counter));
				counter++;
			}
			
		}
	}
	
//	Testa om dubbletter uppstår när leken blandas
	@Test
	public void testDoublesShuffle() {
		deck.shuffle();
		ArrayList<Card> cards = deck.getCards();
		for (int i = 0; i < 51; i++) {
			int counter=i+1;
			while (counter < 52) {
				assertNotEquals(cards.get(i), cards.get(counter));
				counter++;
			}
			
		}
	}
	
//	Testa om scoremetoden räknar om Ess värde då spelaren skulle överstigit 21
	@Test
	public void testScore() {
		ArrayList<Card> cards = deck.getCards();
		deck.score(cards.get(0));
//		Score = 11 om ett Ess räknats med
		assertEquals(11, deck.getScore());
		deck.score(cards.get(13));
//		Score = 12 dvs att ett andra Ess räknas som 1 poäng för att inte överstiga 21
		assertEquals(12, deck.getScore());
	}
	
//	Testa så att shufflemetoden inte ger samma resultat varje gång den anropas
	@Test
	public void testShuffle() {
		deck.shuffle();
		ArrayList<Card> shuffledCards = deck.getCards();
		deck = new Deck();
		deck.shuffle();
		ArrayList<Card> reShuffledCards = deck.getCards();
		assertFalse(shuffledCards.equals(reShuffledCards));
	}
	
//	Testa att objekten i en shufflad Card-array har ett värde - Helst skulle jag haft denna i en annan testklass för att undvika @BeforeEach metoden jag implementerat i denna
//	då med 52 upprepningar
	@RepeatedTest(500)
	public void testDraw() {
		deck.shuffle();
		Card card = deck.draw();
		assertNotNull(card);
	}
	
}