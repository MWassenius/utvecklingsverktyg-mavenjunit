//Mikel Wassenius Java18 test

import java.util.ArrayList;
import java.util.Random;

public class Deck {
	private ArrayList<Card> cards = new ArrayList<Card>();
	private Suit suit;
	private int counter = 0;
	private Integer score = 0;
	
	public Deck() {
		for (int i = 0; i < 4; i++) {
			switch (i) {
			case 0: suit = suit.HEARTS;
				break;
			case 1: suit = suit.SPADES;
				break;
			case 2: suit = suit.DIAMONDS;
				break;
			case 3: suit = suit.CLUBS;
				break;
			}
			for (int j = 1; j < 14; j++) {
				cards.add(counter, new Card(j, suit));
				counter++;
			}
		}
	}
	
	public ArrayList<Card> getCards() {
		return cards;
	}
	
	public void printDeck(ArrayList<Card> cards) {
		for (Card card : cards) {
			System.out.println(card.getValue() + " " + card.getSuit());
		}
	}
	
	public Card draw() {
		Card card = cards.get(0);
		cards.remove(0);
		return card;
	}
	
	public void shuffle() {
		ArrayList<Card> tempCards = new ArrayList<Card>();
		Random random = new Random();
		Card card;
		
		while (cards.size() > 0) {
			card = cards.get(random.nextInt(cards.size()));
			tempCards.add(card);
			cards.remove(card);
		}
		
		cards = tempCards;
	}
	
	 public Integer score(Card card) {
	    	switch (card.getValue()) {				//Switchcase för att returnera rätt värden för korten samt räkna Ess som 1 om spelaren skulle överstiga 21 med Ess värt 11.
			case 1:
				if (score <= 10) {
					score = score + 11;
					return score;
				}
				else {
					score = score + 1;
					return score;
				}
			case 11:
				score = score + 10;
				return score;
			case 12:
				score = score + 10;
				return score;
			case 13:
				score = score + 10;
				return score;
			default:
				score = score + card.getValue();
				return score;
	    	}
	    }
	
	 
	 public int getScore() {
		 return score;
	 }
	 
	 private ArrayList<Card> hand = new ArrayList<Card>();
	 public void hit () {
	    Card card;
	    card = draw();
	    hand.add(card);
	    score(card);
	}
	 
	public ArrayList<Card> getHand() {
		return hand;
	}

}
